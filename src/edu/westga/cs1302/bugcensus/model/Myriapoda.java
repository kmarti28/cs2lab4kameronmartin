/**
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/** Created the Myriapoda class.
 * 
 * @author by Kameron Martin
 * @version Fall 2018
 *
 */
public class Myriapoda extends Bug {
	
	private int numberSegments;

	/**
	 *  The Myriapoda constructor creates a Myriapoda with the specified parameters
	 *  
	 *  @precondition getLength() == length && getNumberLegs() == numberLegs 
	 *  && getColor() == color && getNumberSegments() == numberSegemnts 
	 * 
	 * @param length
	 * 		The length of the Myriapoda
	 * @param numberLegs
	 * 		The number of legs in the Myriapoda
	 * @param color
	 * 		The color of the Myriapoda
	 * @param numberSegments
	 * 		The number of segments in the Myriapoda
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super(length, numberLegs, color);
		
		if (numberSegments < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_SEGMENTS);
		}
	
		this.numberSegments = numberSegments;
	}
	
	/**
	 * Gets the number segments from the bug
	 * 
	 * @precondition none
	 * @postcondition 
	 * 
	 * @return
	 * 		The number of segments of the bug.
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}


	@Override
	public String toString() {
		return "Myriapoda length=" + getLength() + "/#legs=" + getNumberLegs() + "/#segments=" + this.numberSegments + "color=" + getColor();
	}
	
	

}
