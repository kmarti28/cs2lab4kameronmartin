/**
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import javafx.scene.paint.Color;

/**
 * Insect class.
 * 
 * @author by Kameron Martin
 * @version Fall 2018
 *
 */
public class Insect extends Bug {
	
	private Boolean winged;

	private static final int INSECT_LEGS = 6;

	/**
	 *  The Insect constructor creates a insect with the specified parameters
	 * 
	 * @precondition none
	 * @postcondition getLength() == length && getNumberLegs() == 6 
	 * && getColor() == color &&  isWinged() == winged;
	 * 
	 * @param length
	 * 		Length of insect
	 * @param winged
	 * 		decides whether insect is winged or not
	 * @param color
	 * 		Color of insect
	 */
	public Insect(double length, Boolean winged, Color color) {
		super(length, INSECT_LEGS, color);
		
		this.winged = winged;
		
	}
	
	/**
	 * checks if the insect is winged or not
	 * 
	 * @precondition none
	 * @postcondition this.winged() == winged;
	 * 
	 * @return 
	 * 		true if insect has wings, false otherwise.
	 */
	public Boolean isWinged() {
		return this.winged;
	}

	
	@Override
	public String toString() {
		return "Insect length=" + getLength() + " Color=" + getColor() + " winged=" + this.winged;
	}
	
	

}
