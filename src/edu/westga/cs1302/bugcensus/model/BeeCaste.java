package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * Enum for Bee Caste
 * 
 * @author Kameron Martin
 *
 */
public enum BeeCaste {
	
	WORKER, DRONE, QUEEN;
	
	/**
	 * Parses the bee caste.
	 *
	 * @precondition caste != null && BeeCaste.values() contains caste
	 * @postcondition none
	 * 
	 * @param caste the caste to be parsed
	 * @return the Bee caste as caste BeeCaste
	 */
	public static BeeCaste parseCaste(String caste) {
		
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		
		caste = caste.toUpperCase();
		switch (caste) {
			case "WORKER":
				return WORKER;
			case "DRONE":
				return DRONE;
			case "QUEEN":
				return QUEEN;
			default:
				throw new IllegalArgumentException("Invalid caste");
		}
	}
}


