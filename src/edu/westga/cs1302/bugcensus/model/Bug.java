package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class Bug.
 * 
 * @author by Kameron Martin
 * @version Fall 2018
 */
public class Bug {
	
	private double length;
	private int numberLegs;
	private Color color;
	
	/**
	 * The bug constructor creates a bug with the specified parameters
	 * 
	 * @precondition length cannot be <= 0; numberLegs < 0; color != null;
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs && getColor() == color  
	 * 
	 * @param length
	 * 		length of bug
	 * @param numberLegs
	 * 		number of legs
	 * @param color
	 * 		color of bugs
	 */
	public Bug(double length, int numberLegs, Color color) {
		
		if (length <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_LENGTH);
		}
		
		if (numberLegs < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_LEGS);
		}
		
		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		
		this.length = length;
		this.numberLegs = numberLegs;
		this.color = color;
		
	}

	/**
	 * Gets the length from the bug
	 * 
	 * @precondition none
	 * @postcondition getLength() == length
	 * 
	 * @return
	 * 		The length of the bug.
	 */
	public double getLength() {
		return this.length;
	}

	/**
	 * Gets the number of legs from the bug
	 * 
	 * @precondition none
	 * @postcondition getNumberLegs() == numberLegs
	 * 
	 * @return
	 * 		The number of legs of the bug.
	 */
	public int getNumberLegs() {
		return this.numberLegs;
	}

	/**
	 * Gets the color from the bug
	 * 
	 * @precondition none
	 * @postcondition getColor() == color
	 * 
	 * @return
	 * 		The color of the bug.
	 */
	public Color getColor() {
		return this.color;
	}

	@Override
	public String toString() {
		return "Bug length=" + length + ", /#Legs=" + numberLegs + ", color=" + color + "";
	}
	
	
	
	
}
