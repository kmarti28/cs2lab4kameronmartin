/*
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import java.util.ArrayList;
import java.util.Calendar;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensus {
	
	private int year;
	private ArrayList<Bug> bugs;
	private int numberInsects;
	private int numberMyriapodas;
	
	/**
	 * The BugCensus constructor creates a new ArrayList of bugs
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public BugCensus() {
		this.bugs = new ArrayList<Bug>();
		
		this.year = Calendar.getInstance().get(Calendar.YEAR);
		
		this.numberInsects = this.getNumberInsects();
		this.numberMyriapodas = this.getNumberMyriapodas();
	}
	
	/**
	 * The BugCensus constructor creates a new ArrayList of bugs
	 * 
	 * @precondition year cannot be less than 0;
	 * @postcondition this.year() == year;
	 * 
	 * @param year
	 * 		The year the BugCensus was made
	 */
	public BugCensus(int year) {
		this.bugs = new ArrayList<Bug>();
		
		this.numberInsects = this.getNumberInsects();
		this.numberMyriapodas = this.getNumberMyriapodas();
		
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		
		this.year = year;
	}
	
	/**
	 * Gets the year of the BugCensus
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The year of the BugCensus
	 */
	public int getYear() {
		
		return this.year;
	}
	
	/**
	 * Gets the ArrayList of bugs
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The list of bugs in the BugCensus
	 */
	public ArrayList<Bug> getBugs() {
		return this.bugs;
	}
	
	/**
	 * Gets the number of insects 
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The number of insects in BugCensus
	 */
	public int getNumberInsects() {
		int count = 0;
		
		for (Bug currBug: this.bugs) {
			if (currBug.toString().contains("Insect")) {
				count += 1;
			}
		}
		
		return count;
	}
	
	/**
	 * Gets the number of Myriapodas 
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The number of Myriapodas in BugCensus
	 */
	public int getNumberMyriapodas() {
		int count = 0;
		
		for (Bug currBug: this.bugs) {
			if (currBug.toString().contains("Myriapodas")) {
				count += 1;
			}
		}
		
		return count;
	}
	
	/**
	 * Adds a bug into the BugCensus
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param bug
	 * 		The bug added into the census
	 * @return
	 * 		True if successful, false otherwise.
	 */
	public Boolean add(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		
		return this.bugs.add(bug);
	}
	
	/**
	 * The amount of bugs in the Census
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		number of bugs in the Census
	 */
	public int size() {
		return this.bugs.size();
	}
	
	/**
	 * Summary Report of the Bug Census
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The summary report.
	 */
	public String getSummaryReport() {
		
		String output = "Bug Census of " + this.year;
		output += "\nTotal number bugs: " + this.size();
		output += "\nNumber insects: " + this.getNumberInsects();
		output += "\nTotal myriapodias: " + this.getNumberMyriapodas();
		
		return output;
	}
	
}
