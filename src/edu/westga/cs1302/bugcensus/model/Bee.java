/**
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Bee class.
 * @author Kameron Martin
 *
 */
public class Bee extends Insect {
	
	private BeeCaste caste;

	/**
	 * The bee constructor to create bees
	 * 
	 * @precondition caste != null
	 * @postcondition getLength() == length && getNumberLegs() == 6 
	 * && getColor() == Color.BLACK && isWinged() == true && getCaste() == caste 
	 * 
	 * @param length
	 * 		The length of bee
	 * @param caste
	 * 		The type of bee
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, true, Color.BLACK);
		
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		
		this.caste = caste;
	}
	
	/**
	 * Gets the bee Caste from the bee
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return
	 * 		The caste of the bee
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * Sets the BeeCaste to caste
	 * 
	 * @precondition caste != null
	 * @postcondition getCaste() == caste;
	 * 
	 * @param caste
	 * 		The caste you want to set to bee
	 */
	public void setCaste(BeeCaste caste) {
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		
		this.caste = caste;
	}
	
	@Override
	public String toString() {
		return "Bee length=" + this.getLength() + " caste=" + this.getCaste();
	}
	
	

}
