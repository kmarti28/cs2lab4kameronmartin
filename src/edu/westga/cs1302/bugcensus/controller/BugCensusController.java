package edu.westga.cs1302.bugcensus.controller;

import java.io.File;
import edu.westga.cs1302.bugcensus.model.BugCensus;
import edu.westga.cs1302.bugcensus.datatier.CensusDataReader;

/**
 * The Class BugCensus.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensusController {

	/**
	 * Generates a report for the census stored in the specified file
	 * 
	 * @precondition filename != null && filename is not empty
	 * @postcondition none
	 * 
	 * @param filename
	 * 			the name of the input file with bug data
	 */
	public void generateReports(String filename) {
		BugCensus bugCensus = this.readBugCensusFromFile(filename);
		
		System.out.println(bugCensus.getSummaryReport());
		System.out.println();
		
		this.printBugs(bugCensus);
	}

	/**
	 * Reads the bug census from the specified file.
	 * 
	 * @param filename
	 * @return bug census read from file
	 */
	private BugCensus readBugCensusFromFile(String filename) {
		File bugCensusFile = new File(filename);
		CensusDataReader reader = new CensusDataReader(bugCensusFile);
		// TODO: use the reader to read the year and ArrayList of bugs from the
		// bugCensusFile. Then create a new BugCensus object with the respective year
		// and add all bugs to the BugCensus object. Don't forget to return the
		// BugCensus object.

		return null;
	}

	/**
	 * Prints bugs in the specified bug census to console
	 * 
	 * @param bugCensus
	 */
	private void printBugs(BugCensus bugCensus) {
		// TODO: Print all bugs in the specified bug census to the console
	}
}
